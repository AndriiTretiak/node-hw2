const express = require('express');
const router = express.Router();
const {
  createNotes,
  getNotes,
  deleteNote,
  getMyNoteById,
  updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService.js');
const {authMiddleware} = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNotes);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getMyNoteById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
