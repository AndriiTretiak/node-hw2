const express = require('express');
const router = express.Router();
const {
  getUser,
  deleteUser,
  updateUserPassword,
} = require('./usersService.js');

router.get('/users/me', getUser);

router.delete('/users/me', deleteUser);

router.patch('/users/me', updateUserPassword);

module.exports = {
  usersRouter: router,
};
