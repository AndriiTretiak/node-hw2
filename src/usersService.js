const {User} = require('./models/Notes.js');

const getUser = (req, res, next) => {
  try {
    return User.getUser('???').then((result) => {
      res.status(200).json(result);
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

const deleteUser = (req, res, next) => {
  try {
    return User.deleteOne().then((result) => {
      res.status(200).send({message: 'success'});
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

const updateUserPassword = (req, res, next) => {
  try {
    return User.updateUser('username?', req.params.newPassword).then(
        (result) => {
          res.status(200).send({message: 'success'});
        },
    );
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

module.exports = {
  getUser,
  deleteUser,
  updateUserPassword,
};
