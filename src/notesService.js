const {Note} = require('./models/Notes.js');

function getNotes(req, res, next) {
  return Note.find().then((result) => {
    res.json(result.slice(req.params.offset, req.params.limit));
  });
}

function createNotes(req, res, next) {
  const userId = req.user.userId;
  try {
    const {text} = req.body;
    const note = new Note({
      text,
      userId,
    });
    note.save().then((saved) => {
      res.status(200).send({message: 'Success'});
    });
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
}

const getMyNoteById = (req, res, next) => {
  try {
    return Note.find({userId: req.user.userId}, '-__v').then((result) => {
      res.status(200).json(result);
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

const updateMyNoteById = (req, res, next) => {
  try {
    const {text} = req.body;
    return Note.findByIdAndUpdate(
        {_id: req.params.id, userId: req.user.userId},
        {$set: {text}},
    ).then((result) => {
      res.status(200).send({message: 'Success'});
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

const markMyNoteCompletedById = (req, res, next) => {
  try {
    return Note.findByIdAndUpdate(
        {_id: req.params.id, userId: req.user.userId},
        {$set: {completed: true}},
    ).then((result) => {
      res.status(200).json({message: 'Success'});
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

const deleteNote = (req, res, next) => {
  try {
    Note.findByIdAndDelete(req.params.id).then((note) => {
      res.status(200).send({message: 'Success'});
    });
  } catch (err) {
    return res.status(500).send({
      message: 'string',
    });
  }
};

module.exports = {
  createNotes,
  getNotes,
  deleteNote,
  getMyNoteById,
  updateMyNoteById,
  markMyNoteCompletedById,
};
